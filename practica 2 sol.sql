SELECT * FROM ciudad c;
SELECT * FROM compa�ia;
SELECT * FROM persona;
SELECT * FROM supervisa;
SELECT * FROM trabaja;


-- 1
   SELECT 
      COUNT(*)cuenta_ciudades
    FROM
      CIUDAD;
-- 2
  SELECT
      nombre 
  FROM 
      ciudad
  WHERE
      poblaci�n > (SELECT AVG(poblaci�n) FROM ciudad);
-- 3
  SELECT 
      nombre 
  FROM 
    ciudad 
  WHERE
    poblaci�n <(SELECT AVG(poblaci�n) FROM ciudad);

-- 4
  SELECT 
    nombre
  FROM
    ciudad
  WHERE
    poblaci�n=(SELECT MAX(poblaci�n) FROM ciudad);

-- 5
  SELECT 
    nombre
  FROM
      ciudad 
  WHERE 
    poblaci�n= ( SELECT  MIN(poblaci�n) FROM ciudad);
-- 6
  SELECT 
      COUNT(*)
  FROM
    ciudad 
   WHERE 
      poblaci�n>(SELECT AVG(poblaci�n) FROM ciudad c);
-- 7
  SELECT
      ciudad, COUNT(*)    
  FROM
      persona JOIN ciudad c ON c.nombre=ciudad
  GROUP BY ciudad ;
-- 8
  SELECT t.compa�ia, COUNT(*)
   FROM
      trabaja t
    GROUP BY compa�ia;

-- 9 ********************** CAMBIAR CON HAVING *************************

SELECT 
 C1.compa�ia
 FROM 
  (SELECT t.compa�ia, COUNT(*) AS cuenta
    FROM
      trabaja t
    GROUP BY t.compa�ia) AS C1
WHERE 
  C1.cuenta = (SELECT MAX(C2.cuenta) FROM (SELECT t.compa�ia, COUNT(*) AS cuenta
                                              FROM
                                              trabaja t
                                               GROUP BY t.compa�ia)AS C2);


-- 9 con having
  SELECT 
    t.compa�ia
  FROM 
    trabaja t
  GROUP BY t.compa�ia
  HAVING COUNT(*) = (SELECT 
                        MAX(C1.contador) max_contador
                          FROM (
                                  SELECT 
                                      compa�ia, COUNT(*) contador
                                  FROM 
                                    trabaja 
                                  GROUP BY compa�ia) C1
                        );
   
-- 10
  SELECT 
    t.compa�ia, AVG(salario)    
    FROM trabaja t
    GROUP BY t.compa�ia ;

-- 11
  SELECT p.nombre, c.poblaci�n 
    FROM ciudad c 
    JOIN persona p 
    ON c.nombre=p.ciudad;

-- 12
  SELECT p.nombre, p.calle, c.poblaci�n 
    FROM 
    ciudad c 
    JOIN persona p 
    ON c.nombre=p.ciudad; 

-- 13 
  
  
  SELECT 
      c2.nombre, 
      c2.ciudad, 
      c.ciudad ciudad_compa�ia 
  FROM 
    compa�ia c 
    JOIN
         (SELECT
             c1.nombre,
             c1.ciudad,
             t.compa�ia 
           FROM 
             trabaja t 
                 JOIN 
                    (SELECT p.nombre,ciudad FROM persona p) AS c1 
                 ON t.persona=c1.nombre) AS c2 
     ON c2.compa�ia=c.nombre;

-- 14 
SELECT p.nombre
  FROM persona p,trabaja t, compa�ia c
  WHERE p.nombre=t.persona
  AND t.compa�ia=c.nombre
  AND c.ciudad =p.ciudad
  ORDER BY p.nombre;


-- 15
SELECT 
  s.persona,
  s.supervisor 
FROM supervisa s;

-- 16 Forma 1
  
SELECT * FROM supervisa s;
SELECT * FROM persona p;

SELECT 
  c1.supervisor,
  c1.persona,
  c1.ciudadsup,
  persona.ciudad
FROM 
  (SELECT 
    s.supervisor, s.persona, ciudad AS ciudadsup 
    FROM
      supervisa s JOIN persona p 
    ON s.supervisor=p.nombre
    ) c1 JOIN persona 
      on persona=nombre;
-- 16 Forma 2


SELECT c2.supervisor,c2.persona,c2.ciudad ciudadsup, p.ciudad FROM (
  SELECT
    s.supervisor,
    persona,
    ciudad
 FROM 
    supervisa s JOIN persona p 
  ON s.supervisor=p.nombre) c2 JOIN persona p 
  ON c2.persona=nombre;

-- 17
SELECT COUNT(*) FROM (SELECT DISTINCT c.ciudad FROM compa�ia c) AS c1;
  
-- 18 
SELECT COUNT(*) FROM (SELECT DISTINCT p.ciudad  FROM persona p) AS c1;
-- 19
SELECT 
    t.persona 
  FROM 
      trabaja t
  WHERE 
      t.compa�ia='fagor';

-- 20 
  SELECT 
    t.persona 
  FROM 
    trabaja t
  WHERE 
    t.compa�ia<>'fagor';

-- 21
SELECT 
    t.persona 
  FROM 
    trabaja t
  WHERE 
    t.compa�ia='indra';

  -- 22
SELECT 
  t.persona 
FROM 
  trabaja t
 WHERE 
    t.compa�ia='fagor'
    OR
    t.compa�ia='indra'; 
-- 23
  SELECT
    c1.poblaci�n, 
    t.salario, 
    c1.nombre,
    t.compa�ia
FROM 
  trabaja t 
    JOIN  (
            SELECT 
              c.poblaci�n,
              p.nombre 
            FROM  ciudad c 
              JOIN persona p 
              ON c.nombre=ciudad) AS c1 
    ON nombre=persona;